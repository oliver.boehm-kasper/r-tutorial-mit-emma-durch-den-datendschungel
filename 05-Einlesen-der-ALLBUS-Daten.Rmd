# Einlesen und Sichtung der ALLBUS-Daten

## Einlesen der Daten

Zunächst müssen die auszuwertenden Daten im Projekt abgespeichert sein. Emma hat sich die ALLBUS-Daten von der [GESIS-Homepage](https://search.gesis.org/research_data/ZA5280) nach einmaliger Anmeldung heruntergeladen. Du kannst Dir die Daten aus diesem [Online-Ordner](https://gitlab.ub.uni-bielefeld.de/oliver.boehm-kasper/r-tutorial-mit-emma-durch-den-datendschungel/-/blob/main/Daten/ALLBUS_21.sav) (GitLab-Repositorium) downloaden. Danach speichert Du die Daten (ALLBUS_21.sav) im Unterordner "Daten" in Deinem R-Projekt "Datenanalyse ALLBUS". (Wenn Du nicht mehr weißt, wie man Unterordner im R-Projekt anlegt, schaue Dir noch einmal Kapitel 3 dieses Tutorials an.)

Zum Einlesen von Datensätzen stehen in RStudio verschiedene Möglichkeiten zur Verfügung: Im oberen rechten Bereich unter [**Environment**] findest Du den Menüeintrag "Import Dataset".

Der originale Datensatz wurde im SPSS-Format (SPSS ist ein kommerzielles Datenauswertungsprogramm) erstellt. Daher ist die Option "Import Dataset" -\> "From SPSS" zu wählen.

![](Bilder/05_Einlesen%20ALLBUS%20Daten/01.PNG){width="695"}

Es öffnet sich folgendes Fenster:

![](Bilder/05_Einlesen%20ALLBUS%20Daten/02.PNG){width="695"}

Nach Klicken des Buttons "Browse" kann der Speicherort der einzulesenden Datendatei ausgewählt werden. Im erstellten R-Projekt (siehe Kapitel 3) ist der Datensatz im Unterordner "Daten" zu finden. Danach solltest Du überprüfen, ob im unteren linken Bereich "Import Options" unter "Name" die Bezeichnung "ALLBUS_21" steht.

*Achtung: In der Option "Name" sollte wie hier gezeigt "ALLBUS_21" stehen. Die genaue Bezeichnung des Datensatzes ist wichtig, damit die weiteren Schritte in diesem Tutorial funktionieren.*

Nach Klicken des Buttons "Import" wird der Datensatz in R importiert und in ein für R lesbares Datenformat umgewandelt. Der Datensatz ist nun im [**Environment**] sichtbar. Der Datensatz umfasst 5342 Observationen (im Falle der ALLBUS-Befragung sind dies einzelne Personen) und 542 Variablen (Merkmale), zu denen die Befragten Auskunft gegeben haben bzw. die durch den Datenerhebungsprozess gesammelt wurden.

![](Bilder/05_Einlesen%20ALLBUS%20Daten/03.png)

Alternativ kann das Einlesen eines Datensatzes auch per R-Befehl erfolgen. Auf diese Weise kann das Vorgehen besser festgehalten und auch wiederholt durchgeführt werden. Dazu kann der nachfolgende Code genutzt werden. Zunächst wird das für das Einlesen von Datensätzen notwendige Paket *haven* mit dem *library*-Befehl aktiviert. In der nachfolgenden Zeile wird das neue Datenobjekt "ALLBUS_21" benannt. Danach folgt der sogenannte Zuweisungspfeil **\<-**, der im Programm R für die Erzeugung von Objekten (hier eine Datenmatrix) genutzt wird. Hinter dem Zuweisungspfeil steht der *read_sav*-Befehl, gefolgt von der Angabe des Speicherorts der originalen Datendatei ("ALLBUS_21.sav").

```{r}
library(haven)
ALLBUS_21 <- read_sav("Daten/ALLBUS_21.sav") #Einlesen des SPSS-Datensatzes
```

## Sichtung des Datensatzes

Im nächsten Schritt kann man sich einen ersten Überblick über den Datensatz verschaffen. Dafür kann das Paket s*jPlot* genutzt werden. Dieses Paket muss zuvor installiert werden. Wie das funktioniert, hast Du bereits im Kapitel 4 gelernt.

Nach dem Herunterladen des Paketes wird es durch den *library*-Befehl aktiviert. Danach wird der Befehl *view_df* verwendet, um sich eine Beschreibung für den Datensatz anzeigen zu lassen. Die Beschreibung der Inhalte des Datensatzes (Variablennamen, Variablen-Label, Ausprägungen der Variablen und ihre Label) öffnet sich im unten rechten Fenster im Bereich [**Viewer**].

```{r eval=FALSE, message=FALSE, warning=FALSE, include=TRUE}
library(sjPlot)
view_df(ALLBUS_21) #Erstellung einer Datensatzbeschreibung. Diese wird im Viewer-Fenster ausgegeben.
```

![](Bilder/05_Einlesen%20ALLBUS%20Daten/04.png){width="695"}
